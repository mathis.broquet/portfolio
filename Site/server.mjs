import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';

const app = express();
const PORT = 3000;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'resources', 'html', 'index.html'));
});

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'resources', 'html', 'contact.html'));
});

// Middleware pour rediriger les URLs spécifiques
app.use((req, res, next) => {
    console.log("middle")
    if (req.url === '/html/index.html') {
        res.redirect(301, '/');
    } else if (req.url === '/html/contact.html') {
        res.redirect(301, '/contact');
    } else {
        next();
    }
});


// Définir le dossier public pour les fichiers statiques (CSS, JS, images)
app.use('/css', express.static(path.join(__dirname, 'resources', 'css')));
app.use('/images', express.static(path.join(__dirname, 'resources', 'images')));

// Démarrer le serveur
app.listen(PORT, () => {
    console.log(`Serveur en cours d'exécution sur http://localhost:${PORT}`);
});
